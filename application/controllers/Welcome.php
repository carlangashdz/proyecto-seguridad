<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends MY_Controller
{

    public function index()
    {
        $this->load->view('componentes/encabezado');
        $this->load->view('componentes/menu');
        $this->load->view('componentes/piePagina');
    }
}