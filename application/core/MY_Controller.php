<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	function __construct(){
        parent::__construct();
        $this->load->helper('url');		// Para base_url()
        $this->load->helper('text');	// Para el word_limiter, etc.
        $this->load->library('form_validation');	// Para la validación de las formas
        $this->load->library('session');
        $this->load->helper('utilerias_helper');
        $this->load->library('pagination');


        $this->form_validation->set_message('integer',"El campo %s sólo acepta valores enteros");
        $this->form_validation->set_message('required',"El campo %s es requerido");
        $this->form_validation->set_message('min_length',"El campo %s debe tener al menos %s caracteres");
        $this->form_validation->set_message('max_length',"El campo %s debe tener hasta %s caracteres");
        $this->form_validation->set_message('exact_length',"El campo %s debe tener exáctamente %s caracteres");
        $this->form_validation->set_message('validaAlfanumericoAcentosEspacio','El campo %s sólo permite letras, números, acentos, espacios, guión medio, guión bajo, punto, dos puntos, la arroba y el ampersand.');
        $this->form_validation->set_message('validaUsername','El campo %s sólo permite letras, números, punto');
        $this->form_validation->set_message('matches','El campo %s no coincide con el campo %s');
        $this->form_validation->set_message('valid_email','El campo %s debe contener una dirección de correo electrónica válida');
        $this->form_validation->set_message('alpha_dash','El campo %s debe contener sólo caracteres alfanúmericos, guiones medios y guiones bajos');
    }

    public function obtenEntrada($entrada){
    	$resultado = $this->input->get($entrada,TRUE);
    	if($resultado == ""){
    		$resultado = $this->input->post($entrada,TRUE);
    	}
    	return $resultado;
    }

    public function validaAlfanumericoAcentosEspacio($texto=""){
    	if(preg_match("/(^$|^[a-z 0-9áéíóúñÁÉÍÓÚÑ&-\.:,_@]+$)/i",$texto))
        	    return TRUE;
      	return FALSE;
    }

    public function validaUsername($texto=""){
        if(preg_match("/(^$|^[a-z0-9\.]+$)/i",$texto))
                return TRUE;
        return FALSE;
    }

    protected function mostrarPaginaError($paginaActual="",$titulo="Error",$mensajeError="Error general"){
        $datos['paginaActual'] = $paginaActual;
        $datos['titulo'] = $titulo;
        $datos['mensajeError'] = $mensajeError;
        $this->load->view('componentes/encabezado',$datos);
        $this->load->view('componentes/menuAdministracion',$datos);
        $this->load->view('errors/errorGeneral',$datos);
        $this->load->view('componentes/piePagina');
    }

}


/**
 * 
 */
class PidePassword extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model("UsuarioModelo");
        $valido = $this->UsuarioModelo->revisaSesion($this->session->userdata('username'),$this->session->userdata('sesion'));

        //die(print_r($this->router));

        if($valido === FALSE && $this->router->class == 'seguridad' && $this->router->method != 'index')
            redirect(base_url()."index.php/seguridad/index");
        else if($valido === FALSE && $this->router->class != 'seguridad')
            redirect(base_url()."index.php/seguridad/index");
    }
}


